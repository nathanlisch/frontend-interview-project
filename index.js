const https = require('https');

main();

function main() {
    const cards = [];
    makeDeck().then(data => {
        return wait(data);
    }).then(data => {
        return iterateDraw(data.deck_id, []);
    }).then(data => {
        return prepareResponse(data);
    }).then(data => {
        console.log(`SPADES: [${data[0].join(', ')}]`);
        console.log(`CLUBS: [${data[1].join(', ')}]`);
        console.log(`HEARTS: [${data[2].join(', ')}]`);
        console.log(`DIAMONDS: [${data[3].join(', ')}]`);
    })
}

function iterateDraw(deckID, cards) {
    return new Promise((resolve, reject) => {
        drawTwo(deckID).then(data => {
            cards.push(data.cards[0]);
            cards.push(data.cards[1]);

            if (has4Queens(cards)) {
                resolve(cards.map(card => card.code));
            } else {
                setTimeout(() => {
                    resolve(iterateDraw(deckID, cards));
                }, 1000)
            }
        })
    });
}

function prepareResponse(cards) {
    return [
        cards.filter(card => card[1] === 'S').map(card => mapCode(card[0])).sort(sortCards),
        cards.filter(card => card[1] === 'C').map(card => mapCode(card[0])).sort(sortCards),
        cards.filter(card => card[1] === 'H').map(card => mapCode(card[0])).sort(sortCards),
        cards.filter(card => card[1] === 'D').map(card => mapCode(card[0])).sort(sortCards),
    ]
}

function sortCards(cardA, cardB) {
    const order = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'JACK', 'QUEEN', 'KING', 'ACE'];
    if (order.indexOf(cardA) < order.indexOf(cardB)) {
        return -1;
    } else if (order.indexOf(cardA) > order.indexOf(cardB)) {
        return 1;
    } else {
        return 0;
    }
}

function mapCode(code) {
    const map = {
        '0': '10',
        'J': 'JACK',
        'Q': 'QUEEN',
        'K': 'KING',
        'A': 'ACE',
    }
    return map[code] || code;
}


function has4Queens(cards) {
    return cards.filter(card => card.code[0] === 'Q').length === 4;
}

function makeDeck() {
    return get(`${baseUrl}/deck/new/shuffle/?deck_count=1`);
}

function drawTwo(deckID) {
    return get(`${baseUrl}/deck/${deckID}/draw/?count=2`);
}

function get(url) {
    const baseUrl = 'https://deckofcardsapi.com/api';
    return new Promise((resolve, reject) => {
         https.get(url, res => {
            res.setEncoding('utf8');
            let responseBody = '';
            res.on('data', data => {
                responseBody += data;
            });
            res.on('end', () => {
                responseBody = JSON.parse(responseBody);
                resolve(responseBody);
            })
        })
    });
}

function wait(data) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(data);
        }, 1000);
    });
}